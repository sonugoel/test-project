package com.test.people.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.test.people.R;
import com.test.people.databinding.MainActivityBinding;
import com.test.people.view.fragment.PeopleFragment;

/**
 * The main activity of the app
 */
public class MainActivity extends AppCompatActivity {

    MainActivityBinding mMainActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMainActivityBinding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        if (mMainActivityBinding.toolbar != null) {
            setSupportActionBar(mMainActivityBinding.toolbar);
            mMainActivityBinding.toolbar.setTitle("People");
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, new PeopleFragment())
                .commit();
    }
}
